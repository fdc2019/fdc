class UniController < ApplicationController
  
  def index
    @circles = Circle.all
  end
  
  def new
    @circle = Circle.new
  end
  
  def create
    
    @circle = Circle.new(uni_name: params[:uni_name],email: params[:email],pass: params[:pass],circle_name: params[:circle_name],top_name: params[:top_name])
    if @circle.save
      flash[:info] = 'ユーザを登録完了しました。'
      session[:uni_id] = @circle.id
      redirect_to("/")
    else
      render ("/uni/new")
    end
  end

  def edit
    @uni=Circle.find_by(id: params[:id])
  end

  def update
    @uni = Circle.find_by(id: params[:id])
    @uni.uni_name = params[:uni_name]
    @uni.email = params[:email]
    @uni.pass = params[:pass]
    @uni.circle_name = params[:circle_name]
    @uni.top_name = params[:top_name]
    if @uni.save
      flash[:info] = 'ユーザを更新しました。'
      redirect_to("/")
    else
      flash[:info] = 'ユーザを更新できませんでした。'
      redirect_to("/")
    end
  end
  
  
  def destroy
    @uni = Circle.find(params[:id])
    @events=Event.find_by(event_id: session[:uni_id].to_i)
    @uni.destroy
    if @events
      @events.destroy
    end
    session[:uni_id] = nil
    flash[:info]="退会しました"
    redirect_to ("/")
  end
  



  def login
    @uni = Circle.find_by(email: params[:email], pass: params[:pass])
    if @uni
      flash[:info] ="ログインしました"
      session[:uni_id] = @uni.id
      redirect_to "/"
    else
      flash[:info] = "メールアドレスまたはパスワードが間違っています"
      render ("uni/login_form") 
    end
  end
  
  def logout
    session[:uni_id] = nil
    flash[:info]="ログアウトしました"
    redirect_to "/"
  end
  
  def show
    @uni = Circle.find_by(id: params[:id])
    @events = Event.all
  end
end
