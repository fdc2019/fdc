class EventController < ApplicationController
  def index
    @events = Event.all
  end
  
  def new
    @event = Event.new
  end
  
  def create
    @event = Event.new(event_name: params[:event_name], message: params[:message])
    @event.event_id=session[:uni_id]
    if @event.save
      flash[:info] = 'イベントを登録完了しました。'
      redirect_to event_index_path
    else
      flash[:info] = 'イベントを登録できませんでした。'
      render ("/event/new")
    end
  end

  def destroy
    @event = Event.find(params[:id])
    @event.destroy
    redirect_to ("/event/index")
  end
  
  def show
    @event = Event.find_by(id: params[:id])
    @uni=Circle.find_by(id: @event.event_id.to_i)
  end
  
  def edit
    @event=Event.find_by(id: params[:id])
  end
  
  def update
    @event = Event.find_by(id: params[:id])
    @event.event_name = params[:event_name]
    @event.message = params[:message]
    if @event.save
      flash[:info] = 'イベントを更新しました。'
      redirect_to("/event/#{@event.id}/show")
    else
      flash[:info] = 'イベントを更新できませんでした。'
      redirect_to("/event/#{@event.id}/show")
    end
  end
  
end





#model event
      #event_name
      #event_id
      #message