Rails.application.routes.draw do
  
  get "/" => 'uni#index'
  post "uni/create" => "uni#create"
  get "uni/new" => "uni#new"
  
  get 'uni/login' => 'uni#login_form'
  post 'uni/login'
  get 'uni/logout'
  
  get "/event/index" => "event#index"
  post"/event/create" => "event#create"
  get"/event/new" => "event#new"
  
  
  get "uni/:id/edit" => "uni#edit"
  post "uni/:id/update" => "uni#update"
  get "uni/:id/show" => "uni#show"
  get "uni/:id/destroy" => "uni#destroy"
  
  
  get "event/:id/edit" => "event#edit"
  post "event/:id/update" => "event#update"
  get "event/:id/show" => "event#show"
  get "event/:id/destroy" => "event#destroy"
  
end
